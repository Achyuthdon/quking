import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_new_demo/home.dart';
import 'package:flutter_new_demo/otp_card.dart';
import 'package:flutter_new_demo/style.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

import 'imagebanner.dart';

class MyHomePage extends StatelessWidget {
  // This widget is the root of your application.
  static const double _hpad = 20.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('***My Home***')),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/orange_top_shape.png',
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(50, 20.0, 50, 4.0),
                        child: Text("We have sent an OTP to your mobile",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans',
                              fontSize: 23,
                              height: 1.5,
                              color: Colors.black,
                            ),
                            textAlign: TextAlign.center),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(30, 13.0, 30, 4.0),
                        child: Text(
                            "Please check your mobile number +9181****81 to view the OTP",
                            style: TextStyle(
                              fontFamily: 'OpenSans',
                              fontSize: 13,
                              height: 1.5,
                              color: Color(0xFFaeafaf),
                              fontWeight: FontWeight.normal,
                            ),
                            textAlign: TextAlign.center),
                      ),
                      // Row(
                      //      mainAxisAlignment: MainAxisAlignment.start,
                      //     crossAxisAlignment: CrossAxisAlignment.center,
                      //     children: [OTPinput(), OTPinput(), OTPinput(), OTPinput()]),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                        alignment: Alignment.center,
                        child: OTPTextField(
                          length: 4,
                          width: MediaQuery.of(context).size.width - 100,
                          fieldWidth: 50,
                          style:
                              TextStyle(fontFamily: 'OpenSans', fontSize: 17),
                          textFieldAlignment: MainAxisAlignment.spaceBetween,
                          fieldStyle: FieldStyle.box,
                          onCompleted: (pin) {
                            print("Completed: " + pin);
                          },
                        ),
                      ),

                      Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          direction: Axis.horizontal,
                          children: [
                            Text(
                              "Didn't Receive?",
                              style: Theme.of(context).textTheme.bodyText1,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              width: 2.0,
                            ),
                            Text(
                              "Click to Resend",
                              style: TextStyle(
                                fontFamily: 'OpenSans',
                                fontSize: 13,
                                height: 1.5,
                                color: Color(0xFFff711b),
                                fontWeight: FontWeight.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ]),
                    ])),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20, 0, 20, 30),
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: SizedBox(
                width: 350.0,
                height: 50.0,
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Home()));
                    },
                    child: Text("Submit",
                        style: TextStyle(
                          fontFamily: 'OpenSans',
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        )),
                    style: btnStyle),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
