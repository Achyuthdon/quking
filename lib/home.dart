import 'package:flutter_new_demo/HomeScreen.dart';

import 'mealplanner.dart';
import 'more.dart';
import 'package:flutter/material.dart';

import 'recipe.dart';
import 'today.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // Properties & Variables needed

  int currentTab = 0; // to keep track of active tab index
  final List<Widget> screens = [
    Today(),
    Recipes(),
    MealPlanner(),
    More(),
  ]; // to store nested tabs
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = HomeScreen(); // Our first view in viewport

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      resizeToAvoidBottomInset: false,
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: SizedBox(
        width: 75.0,
        height: 75.0,
        child: FloatingActionButton(
          backgroundColor: Color(0xFFff711b),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.add,
                  size: 30,
                  color: Colors.white,
                ),
                Text(
                  'Add Recipe',
                  style: TextStyle(fontFamily: 'OpenSans',
                    fontSize: 9,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          onPressed: () {},
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 13,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    // padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            HomeScreen(); // if user taps on this dashboard tab will be active
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          currentTab == 0
                              ? "assets/images/today-hvr.png"
                              : "assets/images/today.png",
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          'Today',
                          style: TextStyle(fontFamily: 'OpenSans',
                            fontSize: 10,
                            color:
                                currentTab == 0 ? Color(0xFFff711b) : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    // padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            Recipes(); // if user taps on this dashboard tab will be active
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          currentTab == 1
                              ? "assets/images/recipe-hvr.png"
                              : "assets/images/recipe.png",
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          'Recipes',
                          style: TextStyle(fontFamily: 'OpenSans',
                            fontSize: 10,
                            color:
                                currentTab == 1 ? Color(0xFFff711b) : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),

              // Right Tab bar icons

              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MaterialButton(
                    minWidth: 40,
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            MealPlanner(); // if user taps on this dashboard tab will be active
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          currentTab == 2
                              ? "assets/images/meal-planner-hvr.png"
                              : "assets/images/meal-planner.png",
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          'Meal Planner',
                          style: TextStyle(fontFamily: 'OpenSans',
                            fontSize: 10,
                            color:
                                currentTab == 2 ? Color(0xFFff711b) : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                  //  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    onPressed: () {
                      setState(() {
                        currentScreen =
                            More(); // if user taps on this dashboard tab will be active
                        currentTab = 3;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          currentTab == 3
                              ? "assets/images/more-hvr.png"
                              : "assets/images/more.png",
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          'More',
                          style: TextStyle(fontFamily: 'OpenSans',
                            fontSize: 10,
                            color:
                                currentTab == 3 ? Color(0xFFff711b) : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
