import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_new_demo/Constants/colors.dart';
import 'package:flutter_new_demo/HomeScreen.dart';
import 'package:flutter_new_demo/home.dart';
import 'package:flutter_new_demo/myhomepage.dart';
import 'package:flutter_new_demo/textcustom.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwiperNew extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SwiperWorking(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class SwiperWorking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SwiperState();
  }
}

class SwiperState extends State<SwiperWorking> {
  SwiperController _controller;

  double _progressValue = 1;
  bool isOdd = true;

  String _instruction = " Lightly grease an oven tray with cooking oil spray";

  // bool isBackClicked=false;
  @override
  void initState() {
    _controller = new SwiperController();
  }

  void back() {
    _controller.previous(animation: true);
  }

  void fwd() {
    _controller.next(animation: true);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 400,
            child: Stack(children: [
              Swiper(
                itemCount: 10,
                viewportFraction: 1,
                itemWidth: 300,
                itemHeight: 300,
                scale: 1,
                controller: _controller,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  if (index % 2 != 0)
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        height: 400,
                        child: Center(
                            child:
                                Image.asset("assets/images/sunfloweroil.png")));
                  else
                    return Swiper(
                      itemCount: 3,
                      viewportFraction: 1,
                      scale: 1,
                      itemWidth: 300.0,
                      itemHeight: 300.0,

                      layout: SwiperLayout.STACK,
                      // control: new SwiperControl(),
                      itemBuilder: (context1, pos) {
                        if (pos == 0)
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              height: 300,
                              child: Center(
                                  child: Image.asset(
                                      "assets/images/sunfloweroil.png")));
                        else if (pos == 1)
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              height: 300,
                              child: Center(
                                  child: Image.asset(
                                      "assets/images/chillypowder.png")));
                        else
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              height: 300,
                              child: Center(
                                  child: Image.asset(
                                      "assets/images/turmeric.png")));
                      },
                    );
                },
              ),
              Positioned(
                top: 50,
                child: Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.orange, shape: BoxShape.circle),
                  child: Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          TextCustom(
                              "step", 10, Colors.white, FontWeight.normal),
                          TextCustom('${(_progressValue).round()}/10', 20,
                              Colors.white, FontWeight.bold),
                        ],
                      )),
                ),
              )
            ]),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              child: TextCustom(
                  isOdd
                      ? "Lightly grease an oven tray cooking oil."
                      : "Whisk together turmeric, garlic, chilly powder",
                  14,
                  Colors.black,
                  FontWeight.normal)),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  setState(() {
                  if (_progressValue > 1) {
                      if (isOdd)
                        isOdd = false;
                      else
                        isOdd = true;
                      _progressValue = _progressValue - 1;
                      back();
                    }
                  });
                },
              ),
              SizedBox(
                width: 150,
                child: LinearProgressIndicator(
                  backgroundColor: ColorsQuiking.light_grey,
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
                  value: _progressValue/10,
                ),
              ),
              IconButton(
                icon: const Icon(Icons.arrow_forward_ios),
                onPressed: () {
                  setState(() {

                    if (_progressValue < 10) {
                      if (isOdd)
                        isOdd = false;
                      else
                        isOdd = true;
                      _controller.next(animation: true);
                      _progressValue += 1;
                    }
                  });
                },
              )
            ],
          )
        ],
      ),
    ));
  }
}
