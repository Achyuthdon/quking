import 'package:flutter/material.dart';

const String FontName = 'GreatVibes';

const TitleTextStyle = TextStyle(
  fontFamily: 'OpenSans',
  // fontFamily: FontName,
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

const BodytextStyle = TextStyle(
  fontFamily: 'OpenSans',
  // fontFamily: FontName,
  fontSize: 12.0,
  fontWeight: FontWeight.w300,
  color: Color(0xFFaeafaf),
);

const BodytextStyle1 = TextStyle(
  fontFamily: 'OpenSans',
  // fontFamily: FontName,
  fontSize: 12.0,
  fontWeight: FontWeight.w300,
  color: Color(0xFFff711b),
);

final btnStyle = ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
    backgroundColor: MaterialStateProperty.all<Color>(Color(0xFFff711b)),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
            side: BorderSide(
              color: Color(0xFFff711b),
            ))));
