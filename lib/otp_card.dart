import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class OTPinput extends StatelessWidget {
OTPinput();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
        Container(
          padding: const EdgeInsets.fromLTRB(2.0, 5.0, 2.0, 4.0),
          child:  Padding(
          padding: const EdgeInsets.only(right: 2.0, left: 2.0),
          child: new Container(
          alignment: Alignment.center,
          decoration: new BoxDecoration(
          color: Color.fromRGBO(0, 0, 0, 0.1),
          border: new Border.all(
          width: 1.0,
          color: Color.fromRGBO(0, 0, 0, 0.1)
          ),
          borderRadius: new BorderRadius.circular(4.0)
          ),
          child: new TextField(
          inputFormatters: [
          LengthLimitingTextInputFormatter(1),
    ],
    enabled: false,
    autofocus: false,
    textAlign: TextAlign.center,
    style: TextStyle(fontFamily: 'OpenSans',fontSize: 24.0, color: Colors.black),

    )

    ),
    )
    );


  }
}
