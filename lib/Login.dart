import 'package:flutter/material.dart';
import 'package:flutter_new_demo/HomeScreen.dart';
import 'package:flutter_new_demo/home.dart';
import 'package:flutter_new_demo/myhomepage.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginWorking(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class LoginWorking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginState();
  }
}

class LoginState extends State<LoginWorking> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    'assets/images/orange_top_shape.png',
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Center(
                      child: Text(
                        "Login",
                        style: TextStyle(
                          fontFamily: 'OpenSans',
                          fontSize: 28,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(25, 15, 25, 0),
                    child: Center(
                      child: Text(
                        "Please enter your phone number to receive an OTP to create a new account",
                        style: TextStyle(
                          fontFamily: 'OpenSans',
                          fontSize: 13,
                          height: 1.5,
                          color: Color(0xFFaeafaf),
                          fontWeight: FontWeight.normal,
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(25, 15, 25, 0),
                    decoration: BoxDecoration(
                      color: Color(0xFFEFEFEF),
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 0.5,
                      ),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            padding: EdgeInsets.all(2),
                            // color: Colors.grey.withOpacity(0.4),
                            // width: 10,
                            child: TextField(
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(
                                    fontFamily: 'OpenSans',
                                    color: Color(0xFFaeafaf),
                                    fontSize: 15.0),
                                hintText: 'Phone Number',
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(20, 0, 20, 30),
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 53,
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                  elevation: 3,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyHomePage()));
                  },
                  color: Color(0xFFff711b),
                  highlightColor: Colors.deepOrange,
                  splashColor: Colors.black,
                  focusElevation: 3,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Text("Send",
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
