import 'package:flutter/material.dart';
import 'package:flutter_new_demo/Login.dart';
import 'package:flutter_new_demo/details.dart';
import 'package:flutter_new_demo/home.dart';
import 'package:flutter_new_demo/myhomepage.dart';
import 'package:flutter/services.dart'; //portrait mode
import 'package:flutter_new_demo/swiper.dart';

import 'style.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized(); //portrait mode
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp
  ]); //portrait mode
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color(0xFFff711b))); //status
    return MaterialApp(
      title: 'Quking',
      theme: ThemeData(
          appBarTheme: AppBarTheme(
            textTheme: TextTheme(bodyText1: TitleTextStyle),
          ),
          textTheme: TextTheme(
              subtitle1: TitleTextStyle,
              bodyText1: BodytextStyle,
              bodyText2: BodytextStyle1)),
      home: SwiperNew(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 4), () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Login(),
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/background_img.png'),
                  fit: BoxFit.cover),
            ),
            child: Center(
                child: Image.asset('assets/images/new_logo.png',
                    width: 180, height: 135)),
          ),
        ],
      ),
    );
  }
}
