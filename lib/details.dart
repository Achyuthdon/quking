import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_new_demo/Constants/colors.dart';
import 'package:flutter_new_demo/listrecipe.dart';
import 'package:flutter_new_demo/textcustom.dart';

class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  double _top = 0;
  List<String> ingredients = [
    "all-purpose flour",
    " freshly grated Parmesan",
    "garlic powder",
    "Kosher salt",
    "all-purpose flour",
    " freshly grated Parmesan",
    "garlic powder",
    "Kosher salt",
    "all-purpose flour",
    " freshly grated Parmesan"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NotificationListener(
      onNotification: (notification) {
        if (notification is ScrollUpdateNotification) {
          setState(() {
            _top = _top - notification.scrollDelta / 2;
          });
        }
      },
      child: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          child: Stack(
            children: [
              Positioned(
                top: _top,
                child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child:
                        Image.asset("assets/images/Creamy-Lemon-Chicken.jpg")),
              ),
              ListView(
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(15, 200, 15, 0),
                      width: MediaQuery.of(context).size.width,
                      height: 230.0,
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Text("Creamy Lemon Parmesan Chicken",
                                  style: TextStyle(
                                      fontFamily: 'OpenSans',
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  CircleAvatar(
                                    radius: 17.0,
                                    child: Image.asset(
                                        "assets/images/circle_image.png",
                                        width: 40.0,
                                        height: 40.0),
                                    backgroundColor: Colors.white,
                                  ),
                                  SizedBox(width: 5),
                                  Text("Parvathy benil",
                                      style: TextStyle(
                                          fontFamily: 'OpenSans',
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,

                                          color: Colors.white)),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                 Icon(Icons.add, color: Colors.yellow,),

                                  SizedBox(width: 5),
                                  Text("Follow",
                                      style: TextStyle(
                                          fontFamily: 'OpenSans',
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.yellow)),
                                ],
                              ),
                            ),
                          )
                        ],
                      )),
                  Container(
                      height: 100.0,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      width: double.infinity,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.vertical(
                              top: Radius.circular(30.0))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(children: [
                            TextCustom("Calories", 14.0, Colors.black,
                                FontWeight.normal),
                            TextCustom("242", 14.0, Colors.orange,
                                FontWeight.bold),
                            TextCustom(
                                "kcal", 14.0, Colors.black, FontWeight.normal),
                          ]),
                          Container(height: 100,width: 2,color: ColorsQuiking.light_grey,),
                          Column(children: [
                            TextCustom("Recipe", 14.0, Colors.black,
                                FontWeight.normal),
                            TextCustom("242", 14.0, Colors.orange,
                                FontWeight.bold),
                            TextCustom(
                                "kcal", 14.0, Colors.black, FontWeight.normal),
                          ]),
                          Container(height: 100,width: 2,color: ColorsQuiking.light_grey,),

                          Column(children: [
                            TextCustom("Time", 14.0, Colors.black,
                                FontWeight.normal),
                            TextCustom("60", 14.0, Colors.orange,
                                FontWeight.bold),
                            TextCustom(
                                "minutes", 14.0, Colors.black, FontWeight.normal),
                          ]),
                          Container(height: 100,width: 2,color: ColorsQuiking.light_grey,),

                          Column(children: [
                            TextCustom("rating", 14.0, Colors.black,
                                FontWeight.normal),
                            TextCustom("4.2", 14.0, Colors.orange,
                                FontWeight.bold),
                            TextCustom(
                                "stars", 14.0, Colors.black, FontWeight.normal),
                          ])
                        ],
                      )),
                  Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        width: double.infinity,
                        height: 2,
                        color: ColorsQuiking.light_grey,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextCustom(
                              "Ingredients", 25, Colors.black, FontWeight.bold),
                          SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            fit: FlexFit.loose,
                            child: Container(
                              width: 500,
                              height: 2,
                              color: ColorsQuiking.light_grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: ingredients.length,
                      itemBuilder: (context, pos) {
                        return Padding(
                          padding: EdgeInsets.only(bottom: 20.0),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 20,
                              ),
                              Container(
                                  width: 5,
                                  height: 5,
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      shape: BoxShape.circle)),
                              SizedBox(
                                width: 10,
                              ),
                              TextCustom(ingredients[pos], 14, Colors.black,
                                  FontWeight.normal)
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  // Container(
                  //   color: Colors.white,
                  //   child: Padding(
                  //     padding: const EdgeInsets.all(20.0),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //       children: [
                  //         TextCustom("Nutritional Values", 25, Colors.black,
                  //             FontWeight.bold),
                  //         SizedBox(
                  //           width: 10,
                  //         ),
                  //         Flexible(
                  //           fit: FlexFit.loose,
                  //           child: Container(
                  //             width: 500,
                  //             height: 2,
                  //             color: ColorsQuiking.light_grey,
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  // Container(
                  //   width: 100,
                  //   decoration: BoxDecoration(
                  //       color: ColorsQuiking.light_grey,
                  //
                  //       borderRadius: BorderRadius.vertical(top :Radius.circular(50),bottom :Radius.circular(50))),
                  //   child: Padding(
                  //     padding: const EdgeInsets.all(10.0),
                  //     child: Column(
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //
                  //       children: [
                  //         Container(
                  //           width: 60,
                  //           height: 60,
                  //           decoration: BoxDecoration(
                  //               color: Colors.orange, shape: BoxShape.circle),
                  //           child: Align(
                  //             alignment: Alignment.center,
                  //             child: TextCustom(
                  //                 "266", 23, Colors.white, FontWeight.normal),
                  //           ),
                  //         ),
                  //         TextCustom(
                  //             "Calories", 20, Colors.black, FontWeight.normal),
                  //         TextCustom("kcal", 17, Colors.grey, FontWeight.normal)
                  //       ],
                  //     ),
                  //   ),
                  // )


                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          child: Image.asset(
                            'assets/images/circle_image.png',
                          ),
                          radius: 70,
                        ),
                        SizedBox(height: 20,),
                        TextCustom("Parvathy Benil", 17, Colors.black,
                            FontWeight.bold),
                        SizedBox(height: 20,),
                        TextCustom("Parvathy has always had a passion for great food. Growing up in El Paso, Texas, Ida would spend every minute she could in her mother and grandmother’s kitchens. “In those days, everything was made from scratch. Tortillas, mole, pasole, everything! We never went out to dinner; I was just amazed that they could make everything and make it taste so good!” Her developing passion for food was heightened during her many years living in Europe. ", 13, Colors.black,
                            FontWeight.normal),
                      ],

                    ),
                  ),
                  SizedBox(height: 98,),
                ],

              ),
              Positioned(top: 20,
                left: 10,
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: Color(0xFFff9501),
                    size: 25,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ListRecipe(),
                        ));
                  },
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
