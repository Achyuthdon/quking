import 'package:flutter/material.dart';
import 'package:flutter_new_demo/HomeScreen.dart';

class Today extends StatefulWidget {
  @override
  _TodayState createState() => _TodayState();
}

class _TodayState extends State<Today> {
  double _height = 80.0;

  var _expanded = false;

  double _updateState() {
    setState(() {
      if (_height == 80.0)
        _height = 155.0;
      else
        _height = 80.0;
      if (!_expanded)
        _expanded = true;
      else
        _expanded = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 400),
      curve: Curves.easeIn,
      // constraints: BoxConstraints.expand(height: 200.0),
      height: _height,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Color(0xFFff711b),
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(30.0))),
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Delivering to",
                    style: TextStyle(fontFamily: 'OpenSans',fontSize: 12.0, color: Colors.white)),
                Row(
                  children: [
                    Text("Current Location",
                        style: TextStyle(fontFamily: 'OpenSans',
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0,
                            color: Colors.white)),
                    SizedBox(width: 5.0),
                    Image.asset("assets/images/down.png",
                        width: 12.0, height: 12.0)
                  ],
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Material(
                      color: Color(0xFFff711b),
                      child: InkWell(
                        onTap: () {
                          _updateState();
                        },
                        child: CircleAvatar(
                          radius: 17.0,
                          child: Image.asset("assets/images/search.png",
                              width: 15.0, height: 15.0),
                          backgroundColor: Colors.white,
                        ),
                      )),
                  SizedBox(width: 10),
                  Material(
                      color: Color(0xFFff711b),
                      child: InkWell(
                        onTap: () {},
                        child: CircleAvatar(
                          radius: 17.0,
                          child: Image.asset("assets/images/cart.png",
                              width: 15.0, height: 15.0),
                          backgroundColor: Colors.white,
                        ),
                      )),
                ],
              ),
            ),
          ),
          if (_expanded)
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 37.0,
                  child: TextField(
                    textAlign: TextAlign.start,
                    decoration: new InputDecoration(
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset("assets/images/search.png"),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 10.0,minWidth: 10.0
                        ),
                        suffixIcon: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset("assets/images/more-hvr.png"),
                        ),
                       suffixIconConstraints: BoxConstraints(minHeight: 10.0,minWidth: 10.0
                        ),
                        contentPadding: EdgeInsets.all(4.0),
                        border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(30.0),
                          ),
                        ),
                        filled: true,
                        hintStyle: new TextStyle(fontFamily: 'OpenSans',
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                            color: Colors.grey),
                        hintText: "Search recipes...",
                        fillColor: Colors.white),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
