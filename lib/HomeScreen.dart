import 'dart:developer'; //log console
import 'package:flutter/material.dart';
import 'package:flutter_new_demo/listrecipe.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreenWorking(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeScreenWorking extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreenWorking> {
  @override
  void initState() {
    super.initState();
    // calculateHeight();
  }

  List<Recipesss> choices = const <Recipesss>[
    const Recipesss(
        title: "This week's popular Recipes",
        icon: "assets/images/week_recipe_img.jpg"),
    const Recipesss(
        title: "Dessert Recipes", icon: "assets/images/dessert_img.jpg"),
    const Recipesss(
        title: "Chicken Recipes", icon: "assets/images/chicken_recipe.jpg"),
    const Recipesss(
        title: "Fudgy Chewy Brownies", icon: "assets/images/brownies.jpg"),
  ];

  double _height = 80.0;
  double heightWSA;

  var _expanded = false;

  double heightWSB;

  double heightWST;

  double _updateState() {
    setState(() {
      if (_height == 80.0)
        _height = 155.0;
      else
        _height = 80.0;
      if (!_expanded)
        _expanded = true;
      else
        _expanded = false;
    });
  }

  calculateHeight() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

// Height (without SafeArea)
    var padding = MediaQuery.of(context).padding;
    heightWSA = height - padding.top - padding.bottom;

// Height (without status bar)
    heightWSB = height - padding.top;

// Height (without status and toolbar)
    heightWST = height - padding.top - kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height - -_height,
          child: Stack(
              //overflow: Overflow.visible,
              children: <Widget>[
                AnimatedContainer(
                  duration: Duration(milliseconds: 400),
                  curve: Curves.easeIn,
                  // constraints: BoxConstraints.expand(height: 200.0),
                  height: _height,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Color(0xFFff711b),
                      borderRadius:
                          BorderRadius.vertical(bottom: Radius.circular(30.0))),
                  child: Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Delivering to",
                                style: TextStyle(
                                    fontFamily: 'OpenSans',
                                    fontSize: 12.0,
                                    color: Colors.white)),
                            Row(
                              children: [
                                Text("Current Location",
                                    style: TextStyle(
                                        fontFamily: 'OpenSans',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15.0,
                                        color: Colors.white)),
                                SizedBox(width: 5.0),
                                Image.asset("assets/images/down.png",
                                    width: 12.0, height: 12.0)
                              ],
                            )
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Material(
                                  color: Color(0xFFff711b),
                                  child: InkWell(
                                    onTap: () {
                                      _updateState();
                                    },
                                    child: CircleAvatar(
                                      radius: 17.0,
                                      child: Image.asset(
                                          "assets/images/search.png",
                                          width: 15.0,
                                          height: 15.0),
                                      backgroundColor: Colors.white,
                                    ),
                                  )),
                              SizedBox(width: 10),
                              Material(
                                  color: Color(0xFFff711b),
                                  child: InkWell(
                                    onTap: () {},
                                    child: CircleAvatar(
                                      radius: 17.0,
                                      child: Image.asset(
                                          "assets/images/cart.png",
                                          width: 15.0,
                                          height: 15.0),
                                      backgroundColor: Colors.white,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      if (_expanded)
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Container(
                              height: 37.0,
                              child: TextField(
                                textAlign: TextAlign.start,
                                decoration: new InputDecoration(
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Image.asset(
                                          "assets/images/search.png"),
                                    ),
                                    prefixIconConstraints: BoxConstraints(
                                        minHeight: 10.0, minWidth: 10.0),
                                    // suffixIcon: Padding(
                                    //   padding: const EdgeInsets.all(8.0),
                                    //   child: Image.asset(
                                    //       "assets/images/more-hvr.png"),
                                    // ),
                                    // suffixIconConstraints: BoxConstraints(
                                    //     minHeight: 10.0, minWidth: 10.0),
                                    contentPadding: EdgeInsets.all(4.0),
                                    border: new OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(30.0),
                                      ),
                                    ),
                                    filled: true,
                                    hintStyle: new TextStyle(
                                        fontFamily: 'OpenSans',
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.grey),
                                    hintText: "Search recipes...",
                                    fillColor: Colors.white),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height -
                        (_height + MediaQuery.of(context).padding.top),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        Column(children: <Widget>[
                          Container(
                            width: double.infinity,
                            //  height: MediaQuery.of(context).size.height,
                            margin: EdgeInsets.fromLTRB(15, 20, 15, 15),
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Good Morning Parvathy!",
                                    style: TextStyle(
                                      fontFamily: 'OpenSans',
                                      fontSize: 20,
                                      height: 1.5,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(
                                    "Find your Favourite Recipes",
                                    style: TextStyle(
                                      fontFamily: 'OpenSans',
                                      fontSize: 13,
                                      height: 1.5,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ]),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            //  height: MediaQuery.of(context).size.height,
                            margin: EdgeInsets.fromLTRB(15, 15, 15, 0),
                            height: MediaQuery.of(context).size.height * 0.55,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.grey.withOpacity(0.5),
                                width: 1,
                              ),
                              borderRadius: BorderRadius.circular(25.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 1,
                                  blurRadius: 2,
                                  offset: Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Stack(children: <Widget>[
                              Image.asset(
                                'assets/images/chicken_img.jpg',
                                height:
                                    MediaQuery.of(context).size.height * 0.55,
                                fit: BoxFit.cover,
                                color: Colors.black38,
                                colorBlendMode: BlendMode.hardLight,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.all(25),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        // margin: EdgeInsets.all(10),
                                        //  color: Colors.cyan,
                                        //alignment: Alignment.bottomCenter,
                                        child: Text(
                                          "Recipe \nof the Day",
                                          style: TextStyle(
                                            fontFamily: 'OpenSans',
                                            fontSize: 30,
                                            height: 1.5,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          maxLines: 2,
                                        ),
                                      ),
                                      SizedBox(height: 50),
                                    ]),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                // height: 400,
                                // color: Colors.limeAccent,
                                margin: EdgeInsets.all(25),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        // margin: EdgeInsets.all(10),
                                        //color: Colors.yellow,
                                        child: Row(
                                          children: <Widget>[
                                            CircleAvatar(
                                              child: Image.asset(
                                                'assets/images/circle_image.png',
                                              ),
                                              radius: 13,
                                            ),
                                            SizedBox(width: 5),
                                            Text(
                                              "Parvathy",
                                              style: TextStyle(
                                                fontFamily: 'OpenSans',
                                                fontSize: 12,
                                                color: Colors.grey,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            "Honey Garlic Chicken Breast",
                                            style: TextStyle(
                                              fontFamily: 'OpenSans',
                                              fontSize: 16,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            "Non-Veg",
                                            style: TextStyle(
                                              fontFamily: 'OpenSans',
                                              fontSize: 13,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(width: 15),
                                          CircleAvatar(
                                            backgroundColor: Colors.red,
                                            radius: 2,
                                          ),
                                          SizedBox(width: 15),
                                          Text(
                                            "Spicy",
                                            style: TextStyle(
                                              fontFamily: 'OpenSans',
                                              fontSize: 13,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 20),
                                    ]),
                              )
                            ]),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height * 0.70,
                            child: ListView(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              children: List.generate(
                                choices.length,
                                (index) {
                                  return Recipecard(
                                      choice: choices[index],
                                      item: choices[index],
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  ListRecipe(),
                                            ));
                                      });
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 98,
                          ),
                        ]),
                      ],
                    ),
                  ),
                )
              ]),
        ),
      ),
    );
  }
}

class Recipesss {
  const Recipesss({this.title, this.icon});

  final String title;
  final String icon;
}

class Recipecard extends StatelessWidget {
  const Recipecard(
      {Key key,
      this.choice,
      this.onTap,
      @required this.item,
      this.selected: false})
      : super(key: key);

  final Recipesss choice;
  final VoidCallback onTap;
  final Recipesss item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(choice.title)));
        },
        child: Container(
            width: MediaQuery.of(context).size.width,
            //height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
            color: Colors.white,
            child: Container(
              width: MediaQuery.of(context).size.width,
              //  height: MediaQuery.of(context).size.height,
              // margin: EdgeInsets.fromLTRB(15, 40, 15, 15),
              height: MediaQuery.of(context).size.height * 0.25,

              clipBehavior: Clip.antiAliasWithSaveLayer,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.withOpacity(0.5),
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(25.0),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: <Widget>[
                  Image.asset(
                    choice.icon,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.30,
                    fit: BoxFit.cover,
                    color: Colors.black38,
                    colorBlendMode: BlendMode.hardLight,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    // height: 400,
                    // color: Colors.limeAccent,
                    margin: EdgeInsets.all(25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          // margin: EdgeInsets.all(10),
                          //color: Colors.yellow,
                          child: Text(
                            choice.title,
                            style: TextStyle(
                              fontFamily: 'OpenSans',
                              fontSize: 15,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }
}
