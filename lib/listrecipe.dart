import 'dart:developer'; //log console
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_new_demo/HomeScreen.dart';
import 'package:flutter_new_demo/details.dart';
import 'package:flutter_new_demo/home.dart';

// class ListRecipe extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: ListRecipeWorking(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }

class ListRecipe extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ListRecipeState();
  }
}

class ListRecipeState extends State<ListRecipe> {
  List<Recipessslist> choices = const <Recipessslist>[
    const Recipessslist(
        bannerimg: "assets/images/chickenlist1.jpg",
        icon: "assets/images/circle_image.png",
        name: "Parvathy",
        title: "Creamy lemon Chicken",
        veg: "Non-Veg",
        spicy: "Spicy",
        star: "4.9",
        ratings: "(140 ratings)"),
    const Recipessslist(
        bannerimg: "assets/images/chicken_list2.jpg",
        icon: "assets/images/circle_image.png",
        name: "kavya",
        title: "Pepper Chicken",
        veg: "Non-Veg",
        spicy: "Spicy",
        star: "4.9",
        ratings: "(124 ratings)"),
    const Recipessslist(
        bannerimg: "assets/images/chken4.png",
        icon: "assets/images/circle_image.png",
        name: "Kavyajk",
        title: "Honey Garlic Chicken",
        veg: "Non-Veg",
        spicy: "Spicy",
        star: "4.9",
        ratings: "(120 ratings)"),
  ];
  List<MealItems> griditemsss = const <MealItems>[
    const MealItems(title: "Breakfast", isSelected: true),
    const MealItems(title: "Lunch", isSelected: false),
    const MealItems(title: "Dinner", isSelected: false),
  ];
  List<Course> courses = const <Course>[
    const Course(title: "Soup", isSelected: true),
    const Course(title: "Appetizer", isSelected: false),
    const Course(title: "Starter", isSelected: true),
    const Course(title: "Main Dish", isSelected: true),
    const Course(title: "Side", isSelected: false),
    const Course(title: "Dessert", isSelected: true),
    const Course(title: "Drinks", isSelected: true),
  ];
  List<MealType> meal_type = const <MealType>[
    const MealType(title: "Vegetarian", isSelected: true),
    const MealType(title: "Non-Vegetarian", isSelected: false),
  ];
  List<Taste> taste_type = const <Taste>[
    const Taste(title: "Spicy", isSelected: true),
    const Taste(title: "Non-Spicy", isSelected: false),
  ];
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        titleSpacing: -10,
        title: Text(
          "Chicken Recipes",
          style: TextStyle(
            fontFamily: 'OpenSans',
            fontSize: 19,
            height: 1.5,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(30.0),
            bottomRight: Radius.circular(30.0),
          ),
        ),
        actions: <Widget>[
          Padding(
            child: IconButton(
              icon: CircleAvatar(
                child: Image.asset(
                  'assets/images/filter.png',
                  height: 15,
                ),
                //  radius: 50,
                backgroundColor: Colors.white,
              ),
              onPressed: () {
                showModalBottomSheet(
                    /*  transitionAnimationController: AnimationController(
                      // vsync: vsync,
                      duration: Duration(milliseconds: 20000),
                      reverseDuration: Duration(milliseconds: 1200),
                    ),*/
                    context: context,
                    isScrollControlled: true, // full height of modal
                    backgroundColor: Colors.transparent,
                    builder: (context) => Container(
                        height: MediaQuery.of(context).size.height * 0.65,
                        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(40.0),
                            topRight: const Radius.circular(40.0),
                          ),
                        ),
                        child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(60, 15, 60, 0),
                            //   color: Colors.lime,
                            alignment: Alignment.topCenter,
                            child: Divider(
                              color: Colors.black12,
                              height: 5,
                              indent: 60,
                              endIndent: 60,
                              thickness: 3,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            child: Row(children: <Widget>[
                              Text(
                                "Filter",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w900,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                              Expanded(
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                    Text(
                                      "Reset",
                                      style: TextStyle(
                                        fontSize: 17,
                                        color: Color(0xFFff711b),
                                        // fontWeight: FontWeight.bold,
                                        fontFamily: 'OpenSans',
                                      ),
                                    ),
                                  ])),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 17, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Meal",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            width: MediaQuery.of(context).size.width,
                            //
                            // color: Colors.grey,
                            child: Wrap(
                              // scrollDirection: Axis.vertical,
                              // shrinkWrap: true,
                              // crossAxisCount: 3,
                              children: List.generate(
                                griditemsss.length,
                                (index) {
                                  return GridCard(
                                    griditems: griditemsss[index],
                                    item1: griditemsss[index],
                                    onTap: () {
                                      //  isSelected = true;
                                      setState(() {
                                        if (griditemsss[index].isSelected ==
                                            true) {
                                          print("hai");
                                          RaisedButton(
                                              onPressed: () {},
                                              color: Color(0xFFf78439),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.only(
                                                  topLeft:
                                                      const Radius.circular(
                                                          10.0),
                                                  topRight:
                                                      const Radius.circular(
                                                          10.0),
                                                  bottomRight:
                                                      const Radius.circular(
                                                          10.0),
                                                  bottomLeft:
                                                      const Radius.circular(
                                                          10.0),
                                                ),
                                              ));
                                        }
                                      });
                                    },
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 10, 23, 0),
                            child: Divider(
                              color: Colors.black12,
                              height: 5,
                              thickness: 1,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Course",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.18,
                            // color: Colors.green,
                            child: Wrap(
                              //scrollDirection: Axis.vertical,
                              //shrinkWrap: true,
                              // crossAxisCount: 3,
                              // crossAxisSpacing: 4.0,
                              // mainAxisSpacing: 8.0,
                              children: List.generate(
                                courses.length,
                                (index) {
                                  return CourseCard(
                                    courseitems: courses[index],
                                    courseitem1: courses[index],
                                    onTap: () {},
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 10, 23, 0),
                            child: Divider(
                              color: Colors.black12,
                              height: 5,
                              thickness: 1,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Meal Type",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height * 0.15,
                            //color: Colors.greenAccent,
                            child: Wrap(
                              //scrollDirection: Axis.vertical,
                              //shrinkWrap: true,
                              // crossAxisCount: 3,
                              // crossAxisSpacing: 4.0,
                              // mainAxisSpacing: 8.0,
                              children: List.generate(
                                meal_type.length,
                                (index) {
                                  return MealTypeCard(
                                    mealTypeitems: meal_type[index],
                                    mealTypeitem1: meal_type[index],
                                    onTap: () {},
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 10, 23, 0),
                            child: Divider(
                              color: Colors.black12,
                              height: 5,
                              thickness: 1,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Taste",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            width: MediaQuery.of(context).size.width,
                            // height: MediaQuery.of(context).size.height * 0.15,
                            //color: Colors.greenAccent,
                            child: Wrap(
                              //scrollDirection: Axis.vertical,
                              //shrinkWrap: true,
                              // crossAxisCount: 3,
                              // crossAxisSpacing: 4.0,
                              // mainAxisSpacing: 8.0,
                              children: List.generate(
                                taste_type.length,
                                (index) {
                                  return TasteCard(
                                    TasteTypeitems: taste_type[index],
                                    TasteTypeitem1: taste_type[index],
                                    onTap: () {},
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 15, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Serving",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 20, 23, 0),
                            child: RangerClass(),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 20, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Preparation Time",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 20, 23, 0),
                            child: PreparationClass(),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 20, 23, 0),
                            // color: Colors.green,
                            child: Row(children: <Widget>[
                              Text(
                                "Calories",
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 20, 23, 0),
                            child: CaloriesClass(),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(23, 30, 23, 30),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              child: RaisedButton(
                                padding: EdgeInsets.symmetric(
                                    vertical: 8, horizontal: 30),
                                elevation: 3,
                                onPressed: () {},
                                color: Colors.orange,
                                highlightColor: Colors.deepOrange,
                                splashColor: Colors.black,
                                focusElevation: 3,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30))),
                                child: Text("Apply",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                            ),
                          ),
                              SizedBox(height: 98),
                        ]))));
              },
            ),
            padding: EdgeInsets.all(10),
          )
        ],
        backgroundColor: Color(0xFFff711b),
        elevation: 5.0,
        toolbarHeight: 83,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFFff9501),
            size: 19,
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomeScreen(),
                ));
          },
        ),
      ),
      body: Stack(
          //overflow: Overflow.visible,
          children: <Widget>[
            SingleChildScrollView(
              child: Column(children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(0, 18, 0, 15),
                  color: Colors.white,
                  // clipBehavior: Clip.antiAliasWithSaveLayer,
                  // height: MediaQuery.of(context).size.height,
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: List.generate(
                      choices.length,
                      (index) {
                        return Recipecard(
                            choice: choices[index],
                            item: choices[index],
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Details(),
                                  ));
                            });
                      },
                    ),
                  ),
                ),
                SizedBox(height: 98,),
              ]),
            )
          ]),
    );
  }

  AnimationController createAnimationController(TickerProvider vsync) {
    return AnimationController(
      duration: Duration(milliseconds: 20000),
      reverseDuration: Duration(milliseconds: 1200),
      debugLabel: 'BottomSheet',
      vsync: vsync,
    );
  }
}

class RangerClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new RangerClassState();
  }
}

class RangerClassState extends State<RangerClass> {
  double lowval = 5.0;
  double highval = 20.0;

  // RangeValues values = const RangeValues(10.0, 80.0);
  @override
  Widget build(BuildContext context) {
    return new Row(children: <Widget>[
      Text(
        lowval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 2,
      ),
      SliderTheme(
        data: SliderThemeData(
          trackHeight: 2,
          showValueIndicator: ShowValueIndicator.always,
          thumbColor: Color(0xFFff711b),
          inactiveTrackColor: Color(0xFFf0f1f4),
          activeTickMarkColor: Colors.transparent,
          inactiveTickMarkColor: Colors.transparent,
          activeTrackColor: Color(0xFFff711b),
          rangeThumbShape: RoundRangeSliderThumbShape(enabledThumbRadius: 6),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 6),
          overlayColor: Color(0xFFf89256),
        ),
        child: Container(
            width: MediaQuery.of(context).size.width * 0.55,
            margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
            child: RangeSlider(
              values: RangeValues(lowval, highval),
              min: 0.0,
              max: 30.0,
              divisions: 2,
              labels: RangeLabels(lowval.toString(), highval.toString()),
              onChanged: (_range) => setState(() {
                lowval = _range.start;
                highval = _range.end;
              }),
            )),
      ),
      SizedBox(
        width: 2,
      ),
      Text(
        highval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
    ]);
  }
}

class PreparationClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new PreparationClassState();
  }
}

class PreparationClassState extends State<PreparationClass> {
  double lowval = -5.0;
  double highval = 40.0;

  // RangeValues values = const RangeValues(10.0, 80.0);
  @override
  Widget build(BuildContext context) {
    return new Row(children: <Widget>[
      Text(
        lowval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 5,
      ),
      Text(
        "mins",
        style: TextStyle(
          fontSize: 12,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 2,
      ),
      SliderTheme(
        data: SliderThemeData(
          trackHeight: 2,
          showValueIndicator: ShowValueIndicator.always,
          thumbColor: Color(0xFFff711b),
          inactiveTrackColor: Color(0xFFf0f1f4),
          activeTickMarkColor: Colors.transparent,
          inactiveTickMarkColor: Colors.transparent,
          activeTrackColor: Color(0xFFff711b),
          rangeThumbShape: RoundRangeSliderThumbShape(enabledThumbRadius: 6),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 6),
          overlayColor: Color(0xFFf89256),
        ),
        child: Container(
            width: MediaQuery.of(context).size.width * 0.35,
            margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
            child: RangeSlider(
              values: RangeValues(lowval, highval),
              min: -10.0,
              max: 50.0,
              divisions: 2,
              labels: RangeLabels(lowval.toString(), highval.toString()),
              onChanged: (_range) => setState(() {
                lowval = _range.start;
                highval = _range.end;
              }),
            )),
      ),
      SizedBox(
        width: 2,
      ),
      Text(
        highval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 5,
      ),
      Text(
        "mins",
        style: TextStyle(
          fontSize: 12,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
    ]);
  }
}

class CaloriesClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new CaloriesClassState();
  }
}

class CaloriesClassState extends State<CaloriesClass> {
  double lowval = 5.0;
  double highval = 490.0;

  // RangeValues values = const RangeValues(10.0, 80.0);
  @override
  Widget build(BuildContext context) {
    return new Row(children: <Widget>[
      Text(
        lowval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 5,
      ),
      Text(
        "cal",
        style: TextStyle(
          fontSize: 12,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SliderTheme(
        data: SliderThemeData(
          trackHeight: 2,
          showValueIndicator: ShowValueIndicator.always,
          thumbColor: Color(0xFFff711b),
          inactiveTrackColor: Color(0xFFf0f1f4),
          activeTickMarkColor: Colors.transparent,
          inactiveTickMarkColor: Colors.transparent,
          activeTrackColor: Color(0xFFff711b),
          rangeThumbShape: RoundRangeSliderThumbShape(enabledThumbRadius: 6),
          overlayShape: RoundSliderOverlayShape(overlayRadius: 6),
          overlayColor: Color(0xFFf89256),
        ),
        child: Container(
            width: MediaQuery.of(context).size.width * 0.40,
            margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
            child: RangeSlider(
              values: RangeValues(lowval, highval),
              min: 0.0,
              max: 500.0,
              divisions: 2,
              labels: RangeLabels(lowval.toString(), highval.toString()),
              onChanged: (_range) => setState(() {
                lowval = _range.start;
                highval = _range.end;
              }),
            )),
      ),
      Text(
        highval.toString(),
        style: TextStyle(
          fontSize: 15,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        width: 5,
      ),
      Text(
        "cal",
        style: TextStyle(
          fontSize: 12,
          fontFamily: 'OpenSans',
        ),
        textAlign: TextAlign.center,
      ),
    ]);
  }
}

class MealItems {
  const MealItems({this.title, this.isSelected});
  final String title;
  final bool isSelected;
}

class Course {
  const Course({this.title, this.isSelected});
  final String title;
  final bool isSelected;
}

class MealType {
  const MealType({this.title, this.isSelected});
  final String title;
  final bool isSelected;
}

class Taste {
  const Taste({this.title, this.isSelected});
  final String title;
  final bool isSelected;
}

class GridCard extends StatelessWidget {
  const GridCard(
      {Key key,
      this.griditems,
      this.onTap,
      @required this.item1,
      this.selected: false})
      : super(key: key);

  final MealItems griditems;
  final VoidCallback onTap;
  final MealItems item1;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          child: Column(children: <Widget>[
            Container(
                width: 87.0,
                height: 33.0,
                margin: EdgeInsets.fromLTRB(0, 0, 7, 12),
                child: RaisedButton(
                  onPressed: () {},
                  padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                  elevation: 1,
                  color: Color(0xFFf0f1f4),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(8.0),
                      topRight: const Radius.circular(8.0),
                      bottomRight: const Radius.circular(8.0),
                      bottomLeft: const Radius.circular(8.0),
                    ),
                  ),
                  child: Text(
                    griditems.title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ))
          ]),
        ));
  }
}

class CourseCard extends StatelessWidget {
  const CourseCard(
      {Key key,
      this.courseitems,
      this.onTap,
      @required this.courseitem1,
      this.selected: false})
      : super(key: key);

  final Course courseitems;
  final VoidCallback onTap;
  final Course courseitem1;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          child: Column(children: <Widget>[
            Container(
                width: 87.0,
                height: 33.0,
                margin: EdgeInsets.fromLTRB(0, 0, 7, 12),
                child: RaisedButton(
                  onPressed: () {},
                  padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                  elevation: 1,
                  color: Color(0xFFf0f1f4),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(8.0),
                      topRight: const Radius.circular(8.0),
                      bottomRight: const Radius.circular(8.0),
                      bottomLeft: const Radius.circular(8.0),
                    ),
                  ),
                  child: Text(
                    courseitems.title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ))
          ]),
        ));
  }
}

class MealTypeCard extends StatelessWidget {
  const MealTypeCard(
      {Key key,
      this.mealTypeitems,
      this.onTap,
      @required this.mealTypeitem1,
      this.selected: false})
      : super(key: key);

  final MealType mealTypeitems;
  final VoidCallback onTap;
  final MealType mealTypeitem1;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          child: Column(children: <Widget>[
            Container(
                width: 98.0,
                height: 33.0,
                margin: EdgeInsets.fromLTRB(0, 0, 7, 12),
                child: RaisedButton(
                  onPressed: () {},
                  padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                  elevation: 1,
                  color: Color(0xFFf0f1f4),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(8.0),
                      topRight: const Radius.circular(8.0),
                      bottomRight: const Radius.circular(8.0),
                      bottomLeft: const Radius.circular(8.0),
                    ),
                  ),
                  child: Text(
                    mealTypeitems.title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ))
          ]),
        ));
  }
}

class TasteCard extends StatelessWidget {
  const TasteCard(
      {Key key,
      this.TasteTypeitems,
      this.onTap,
      @required this.TasteTypeitem1,
      this.selected: false})
      : super(key: key);

  final Taste TasteTypeitems;
  final VoidCallback onTap;
  final Taste TasteTypeitem1;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onTap();
        },
        child: Container(
          child: Column(children: <Widget>[
            Container(
                width: 87.0,
                height: 33.0,
                margin: EdgeInsets.fromLTRB(0, 0, 7, 12),
                child: RaisedButton(
                  onPressed: () {},
                  padding: EdgeInsets.fromLTRB(8, 6, 8, 6),
                  elevation: 1,
                  color: Color(0xFFf0f1f4),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(8.0),
                      topRight: const Radius.circular(8.0),
                      bottomRight: const Radius.circular(8.0),
                      bottomLeft: const Radius.circular(8.0),
                    ),
                  ),
                  child: Text(
                    TasteTypeitems.title,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ))
          ]),
        ));
  }
}

class Recipessslist {
  const Recipessslist(
      {this.bannerimg,
      this.icon,
      this.name,
      this.title,
      this.veg,
      this.spicy,
      this.star,
      this.ratings});
  final String bannerimg;
  final String icon;
  final String name;
  final String title;
  final String veg;
  final String spicy;
  final String star;
  final String ratings;
}

class Recipecard extends StatelessWidget {
  const Recipecard(
      {Key key,
      this.choice,
      this.onTap,
      @required this.item,
      this.selected: false})
      : super(key: key);

  final Recipessslist choice;
  final VoidCallback onTap;
  final Recipessslist item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(choice.title)));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        //height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.fromLTRB(15, 0, 15, 13),
        child: Container(
          width: MediaQuery.of(context).size.width,
          //  height: MediaQuery.of(context).size.height,
          // margin: EdgeInsets.fromLTRB(15, 40, 15, 15),
          height: MediaQuery.of(context).size.height * 0.40,
          //color: Colors.grey,
          child: Column(
            children: <Widget>[
              Image.asset(
                choice.bannerimg,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.25,
                fit: BoxFit.cover,
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(15, 5, 8, 2),
                  // color: Colors.black12,
                  child: Row(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 5, 0),
                      child: CircleAvatar(
                        child: Image.asset(
                          choice.icon,
                        ),
                        radius: 13,
                      ),
                    ),
                    //  SizedBox(width: 5),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 10, 0),
                      child: Text(
                        choice.name,
                        style: TextStyle(
                          fontFamily: 'OpenSans',
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Color(0xFFff9501),
                            size: 18,
                          ),
                          SizedBox(width: 1),
                          Text(
                            choice.star,
                            style: TextStyle(
                              fontFamily: 'OpenSans',
                              fontSize: 12,
                              height: 1.5,
                              color: Color(0xFFffb950),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            width: 3,
                          ),
                          Text(
                            choice.ratings,
                            style: TextStyle(
                              fontFamily: 'OpenSans',
                              // wordSpacing: 30,
                              fontSize: 12,
                              height: 1.5,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    )
                  ])),
              Container(
                margin: EdgeInsets.fromLTRB(15, 5, 15, 2),
                child: Row(
                  children: <Widget>[
                    Text(
                      choice.title,
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(15, 5, 15, 2),
                child: Row(
                  children: <Widget>[
                    Text(
                      choice.veg,
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        fontSize: 13,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(width: 15),
                    CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 1.5,
                    ),
                    SizedBox(width: 15),
                    Text(
                      choice.spicy,
                      style: TextStyle(
                        fontFamily: 'OpenSans',
                        fontSize: 13,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
