import 'package:flutter/material.dart';

class TextCustom extends StatelessWidget {
  String _textString;
  double _fontSize = 10.0;
  String _fontFamily = 'OpenSans';
  Color _color = Colors.black;
  FontWeight _fontweight = FontWeight.normal;

  TextCustom(this._textString, this._fontSize, this._color, this._fontweight);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(_textString,
        textAlign: TextAlign.center,
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontSize: _fontSize,
            fontFamily: _fontFamily,
            color: _color,
            fontWeight: _fontweight));
  }
}
