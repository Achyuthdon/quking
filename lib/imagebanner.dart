import 'package:flutter/material.dart';

class ImageBanner extends StatelessWidget {
  final String _assetPath;
  Text text;

  ImageBanner(this._assetPath);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      constraints: BoxConstraints.expand(
        height:320.0)
      ,
      child: Image.asset(_assetPath,fit : BoxFit.cover)
    );
  }
}
